// General
const OFF_WHITE = "#d7d7d7"

// Laser Grid
const PINK = "#FF69B4"
const BLUE = "#3271B8"

export { OFF_WHITE, PINK, BLUE }
