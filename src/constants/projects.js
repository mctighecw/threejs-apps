import galaxyImage from "Assets/projects/galaxy.jpg"
import oceanWavesImage from "Assets/projects/ocean-waves.jpg"
import spaceHelmetImage from "Assets/projects/space-helmet.jpg"
import magicPortalImage from "Assets/projects/magic-portal.jpg"
import laserGridImage from "Assets/projects/laser-grid.jpg"

export default [
  {
    title: "Galaxy",
    description: "A 3D spiral galaxy*",
    url: "/galaxy",
    image: galaxyImage,
  },
  {
    title: "Ocean Waves",
    description: "An ocean with moving waves*",
    url: "/ocean-waves",
    image: oceanWavesImage,
  },
  {
    title: "Space Helmet",
    description: "A sci-fi helmet in the mountains*",
    url: "/space-helmet",
    image: spaceHelmetImage,
  },
  {
    title: "Magic Portal",
    description: "A scene created using Blender*",
    url: "/magic-portal",
    image: magicPortalImage,
  },
  {
    title: "Laser Grid",
    description: "A laser vector grid, inspired by the 1980s",
    url: "/laser-grid",
    image: laserGridImage,
  },
]
