import React, { useEffect } from "react"
import * as THREE from "three"
import * as dat from "dat.gui"
import path from "path"

import { OrbitControls } from "three/examples/jsm/controls/OrbitControls"
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader"
import { DRACOLoader } from "three/examples/jsm/loaders/DRACOLoader"

import firefliesVertexShader from "./shaders/fireflies/vertex.glsl"
import firefliesFragmentShader from "./shaders/fireflies/fragment.glsl"
import portalVertexShader from "./shaders/portal/vertex.glsl"
import portalFragmentShader from "./shaders/portal/fragment.glsl"

import bakedImage from "./files/baked.jpg"
import portalFile from "./files/portal.glb"

const MagicPortal = () => {
  const body = document.querySelector("body")
  body.style.overflow = "hidden"

  useEffect(() => {
    // Canvas
    const canvas = document.querySelector("canvas.magic-portal")

    // Scene
    const scene = new THREE.Scene()

    // Sizes
    const sizes = {
      width: window.innerWidth,
      height: window.innerHeight,
    }

    // GUI
    const gui = new dat.GUI()
    const guiColor = gui.addFolder("colors")
    const guiFF = gui.addFolder("fireflies")

    guiColor.open()
    guiFF.open()

    const parameters = {
      portalInner: "#0ff1ce",
      portalOuter: "#5c43f5",
      background: "#141414",
      firefliesCount: 40,
      firefliesSize: 100,
    }

    /**
     * Loaders
     */
    const loadingManager = new THREE.LoadingManager()
    loadingManager.onError = (err) => {
      console.error(`Loading error: ${err}`)
    }

    // Texture loader
    const textureLoader = new THREE.TextureLoader(loadingManager)

    // Draco loader
    const dracoLoader = new DRACOLoader(loadingManager)
    const dracoPathDev = path.resolve(__dirname, "node_modules/three/examples/js/libs/draco")
    const dracoPathProd = path.resolve(__dirname, "draco")
    const dracoPath = process.env.NODE_ENV === "development" ? dracoPathDev : dracoPathProd
    dracoLoader.setDecoderPath(dracoPath)

    // GLTF loader
    const gltfLoader = new GLTFLoader(loadingManager)
    gltfLoader.setDRACOLoader(dracoLoader)

    /**
     * Textures
     */
    const bakedTexture = textureLoader.load(bakedImage)
    bakedTexture.flipY = false
    bakedTexture.encoding = THREE.sRGBEncoding

    /**
     * Materials
     */
    // Baked material
    const bakedMaterial = new THREE.MeshBasicMaterial({ map: bakedTexture })

    // Pole light material
    const poleLightMaterial = new THREE.MeshBasicMaterial({ color: 0xffffe5 })

    // Portal light materials
    const portalLightMaterial = new THREE.ShaderMaterial({
      uniforms: {
        uTime: { value: 0 },
        uColorStart: { value: new THREE.Color(parameters.portalInner) },
        uColorEnd: { value: new THREE.Color(parameters.portalOuter) },
      },
      vertexShader: portalVertexShader,
      fragmentShader: portalFragmentShader,
    })

    // GUI
    guiColor.addColor(parameters, "portalInner").onChange(() => {
      portalLightMaterial.uniforms.uColorStart.value.set(parameters.portalInner)
    })

    guiColor.addColor(parameters, "portalOuter").onChange(() => {
      portalLightMaterial.uniforms.uColorEnd.value.set(parameters.portalOuter)
    })

    /**
     * Model
     */
    gltfLoader.load(portalFile, (gltf) => {
      scene.add(gltf.scene)

      // Get each object
      const bakedMesh = gltf.scene.children.find((child) => child.name === "baked")
      const portalLightMesh = gltf.scene.children.find((child) => child.name === "portalLight")
      const poleLightAMesh = gltf.scene.children.find((child) => child.name === "poleLightA")
      const poleLightBMesh = gltf.scene.children.find((child) => child.name === "poleLightB")

      // Apply materials
      bakedMesh.material = bakedMaterial
      portalLightMesh.material = portalLightMaterial
      poleLightAMesh.material = poleLightMaterial
      poleLightBMesh.material = poleLightMaterial
    })

    /**
     * Fireflies
     */
    let fireflies = null
    let firefliesGeometry = null
    let firefliesMaterial = null

    const generateFireflies = () => {
      if (fireflies !== null) {
        firefliesGeometry.dispose()
        firefliesMaterial.dispose()
        scene.remove(fireflies)
      }

      const { firefliesCount, firefliesSize } = parameters

      // Geometry
      firefliesGeometry = new THREE.BufferGeometry()
      const positionArray = new Float32Array(firefliesCount * 3)
      const scaleArray = new Float32Array(firefliesCount)

      for (let i = 0; i < firefliesCount; i++) {
        positionArray[i * 3] = (Math.random() - 0.5) * 4
        positionArray[i * 3 + 1] = Math.random() * 1.5
        positionArray[i * 3 + 2] = (Math.random() - 0.5) * 4

        scaleArray[i] = Math.random()
      }

      firefliesGeometry.setAttribute("position", new THREE.BufferAttribute(positionArray, 3))
      firefliesGeometry.setAttribute("aScale", new THREE.BufferAttribute(scaleArray, 1))

      // Material
      firefliesMaterial = new THREE.ShaderMaterial({
        uniforms: {
          uTime: { value: 0 },
          uPixelRatio: { value: Math.min(window.devicePixelRatio, 2) },
          uSize: { value: firefliesSize },
        },
        vertexShader: firefliesVertexShader,
        fragmentShader: firefliesFragmentShader,
        transparent: true,
        blending: THREE.AdditiveBlending,
        depthWrite: false,
      })

      // Points
      fireflies = new THREE.Points(firefliesGeometry, firefliesMaterial)
      scene.add(fireflies)
    }
    generateFireflies()

    // GUI
    guiFF
      .add(parameters, "firefliesCount")
      .min(0)
      .max(500)
      .step(1)
      .name("count")
      .onChange(() => {
        generateFireflies()
      })

    guiFF
      .add(parameters, "firefliesSize")
      .min(1)
      .max(300)
      .step(1)
      .name("size")
      .onChange(() => {
        generateFireflies()
      })

    // Event listeners
    window.addEventListener("resize", () => {
      sizes.width = window.innerWidth
      sizes.height = window.innerHeight

      camera.aspect = sizes.width / sizes.height
      camera.updateProjectionMatrix()

      renderer.setSize(sizes.width, sizes.height)
      renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

      // Update fireflies
      firefliesMaterial.uniforms.uPixelRatio.value = Math.min(window.devicePixelRatio, 2)
    })

    window.addEventListener("dblclick", () => {
      const fullScreen = document.fullscreenElement || document.webkitfullscreenElement

      if (!fullScreen) {
        if (canvas.requestFullscreen) {
          canvas.requestFullscreen()
        } else if (canvas.webkitRequestFullscreen) {
          canvas.webkitRequestFullscreen()
        }
      } else {
        if (document.exitFullscreen) {
          document.exitFullscreen()
        } else if (document.webkitExitFullscreen) {
          document.webkitExitFullscreen()
        }
      }
    })

    // Base camera
    const camera = new THREE.PerspectiveCamera(45, sizes.width / sizes.height, 0.1, 100)
    camera.position.x = 4
    camera.position.y = 2
    camera.position.z = 4
    scene.add(camera)

    // Controls
    const controls = new OrbitControls(camera, canvas)
    controls.enableDamping = true

    // Renderer
    const renderer = new THREE.WebGLRenderer({
      canvas: canvas,
      antialias: true,
    })
    renderer.outputEncoding = THREE.sRGBEncoding
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
    renderer.setClearColor(parameters.background)

    // GUI
    guiColor.addColor(parameters, "background").onChange(() => {
      renderer.setClearColor(parameters.background)
    })

    // Animate
    const clock = new THREE.Clock()

    const tick = () => {
      const elapsedTime = clock.getElapsedTime()

      // Update materials
      portalLightMaterial.uniforms.uTime.value = elapsedTime

      if (firefliesMaterial !== null) {
        firefliesMaterial.uniforms.uTime.value = elapsedTime
      }

      controls.update()
      renderer.render(scene, camera)
      window.requestAnimationFrame(tick)
    }

    tick()
  }, [])

  return <canvas className="magic-portal"></canvas>
}

export default MagicPortal
