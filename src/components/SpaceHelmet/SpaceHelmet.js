import React, { useEffect } from "react"
import * as THREE from "three"
import * as dat from "dat.gui"
import path from "path"

import { OrbitControls } from "three/examples/jsm/controls/OrbitControls"
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader"
import { EffectComposer } from "three/examples/jsm/postprocessing/EffectComposer"
import { RenderPass } from "three/examples/jsm/postprocessing/RenderPass"
import { ShaderPass } from "three/examples/jsm/postprocessing/ShaderPass"
import { SMAAPass } from "three/examples/jsm/postprocessing/SMAAPass"
import { UnrealBloomPass } from "three/examples/jsm/postprocessing/UnrealBloomPass"

import tintFragmentShader from "./shaders/tint/fragment.glsl"
import tintVertexShader from "./shaders/tint/vertex.glsl"
import displacementFragmentShader from "./shaders/displacement/fragment.glsl"
import displacementVertexShader from "./shaders/displacement/vertex.glsl"

const SpaceHelmet = () => {
  const body = document.querySelector("body")
  body.style.overflow = "hidden"

  useEffect(() => {
    // Canvas
    const canvas = document.querySelector("canvas.space-helmet")

    // Scene
    const scene = new THREE.Scene()

    // Sizes
    const sizes = {
      width: window.innerWidth,
      height: window.innerHeight,
    }

    // GUI
    const gui = new dat.GUI()

    // Loaders
    const loadingManager = new THREE.LoadingManager()
    loadingManager.onError = (err) => {
      console.error(`Loading error: ${err}`)
    }

    const gltfLoader = new GLTFLoader(loadingManager)
    const cubeTextureLoader = new THREE.CubeTextureLoader(loadingManager)
    const textureLoader = new THREE.TextureLoader(loadingManager)

    const updateAllMaterials = () => {
      scene.traverse((child) => {
        if (child instanceof THREE.Mesh && child.material instanceof THREE.MeshStandardMaterial) {
          child.material.envMapIntensity = 5
          child.material.needsUpdate = true
          child.castShadow = true
          child.receiveShadow = true
        }
      })
    }

    // Asset file paths
    const filePathDev = path.resolve(__dirname, "src/components/SpaceHelmet/files")
    const filePathProd = path.resolve(__dirname, "assets")
    const filePath = process.env.NODE_ENV === "development" ? filePathDev : filePathProd

    // Environment map
    const environmentMap = cubeTextureLoader.load([
      `${filePath}/envMap/px.jpg`,
      `${filePath}/envMap/nx.jpg`,
      `${filePath}/envMap/py.jpg`,
      `${filePath}/envMap/ny.jpg`,
      `${filePath}/envMap/pz.jpg`,
      `${filePath}/envMap/nz.jpg`,
    ])

    environmentMap.encoding = THREE.sRGBEncoding

    scene.background = environmentMap
    scene.environment = environmentMap

    // Models
    gltfLoader.load(`${filePath}/damagedHelmet/glTF/DamagedHelmet.gltf`, (gltf) => {
      gltf.scene.scale.set(2, 2, 2)
      gltf.scene.rotation.y = Math.PI * 0.5
      scene.add(gltf.scene)

      updateAllMaterials()
    })

    // Lights
    const directionalLight = new THREE.DirectionalLight("#fff", 3)
    directionalLight.castShadow = true
    directionalLight.shadow.mapSize.set(1024, 1024)
    directionalLight.shadow.camera.far = 15
    directionalLight.shadow.normalBias = 0.05
    directionalLight.position.set(0.25, 3, -2.25)
    scene.add(directionalLight)

    // Base camera
    const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.1, 100)
    camera.position.set(4, 1, -4)
    scene.add(camera)

    // Controls
    const controls = new OrbitControls(camera, canvas)
    controls.enableDamping = true

    // Renderer
    const renderer = new THREE.WebGLRenderer({
      canvas: canvas,
      antialias: true,
    })
    renderer.shadowMap.enabled = true
    renderer.shadowMap.type = THREE.PCFShadowMap
    renderer.physicallyCorrectLights = true
    renderer.outputEncoding = THREE.sRGBEncoding
    renderer.toneMapping = THREE.ReinhardToneMapping
    renderer.toneMappingExposure = 1.5
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

    /**
     * Post processing
     */
    let RenderTargetClass = null
    const useMultisampling = renderer.getPixelRatio() === 1 && renderer.capabilities.isWebGL2

    if (useMultisampling) {
      RenderTargetClass = THREE.WebGLMultisampleRenderTarget
    } else {
      RenderTargetClass = THREE.WebGLRenderTarget
    }

    const renderTarget = new RenderTargetClass(800, 600, {
      minFilter: THREE.LinearFilter,
      magFilter: THREE.LinearFilter,
      format: THREE.RGBAFormat,
      encoding: THREE.sRGBEncoding,
    })

    const effectComposer = new EffectComposer(renderer, renderTarget)
    effectComposer.setSize(sizes.width, sizes.height)
    effectComposer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

    // Passes
    const renderPass = new RenderPass(scene, camera)
    effectComposer.addPass(renderPass)

    // unrealBloomPass
    const unrealBloomPass = new UnrealBloomPass()
    unrealBloomPass.enabled = false
    unrealBloomPass.strength = 0.5
    unrealBloomPass.radius = 1
    unrealBloomPass.threshold = 0.6
    effectComposer.addPass(unrealBloomPass)

    // GUI
    const guiUBP = gui.addFolder("unrealBloomFilter")
    guiUBP.add(unrealBloomPass, "enabled")
    guiUBP.add(unrealBloomPass, "strength").min(0).max(2).step(0.001)
    guiUBP.add(unrealBloomPass, "radius").min(0).max(2).step(0.001)
    guiUBP.add(unrealBloomPass, "threshold").min(0).max(1).step(0.001)
    guiUBP.open()

    // Tint pass
    const tintShader = {
      uniforms: {
        tDiffuse: { value: null },
        uTint: { value: null },
      },
      vertexShader: tintVertexShader,
      fragmentShader: tintFragmentShader,
    }

    const parametersTp = {
      reset: () => null,
    }

    const tintPass = new ShaderPass(tintShader)
    tintPass.material.uniforms.uTint.value = new THREE.Vector3()
    effectComposer.addPass(tintPass)

    // GUI
    const guiTP = gui.addFolder("tintFilter")
    guiTP.add(tintPass.material.uniforms.uTint.value, "x").min(-1).max(1).step(0.001).name("red")
    guiTP.add(tintPass.material.uniforms.uTint.value, "y").min(-1).max(1).step(0.001).name("green")
    guiTP.add(tintPass.material.uniforms.uTint.value, "z").min(-1).max(1).step(0.001).name("blue")
    guiTP.add(parametersTp, "reset").onChange(() => {
      tintPass.material.uniforms.uTint.value.set(0, 0, 0)
    })
    guiTP.open()

    // Displacement pass
    const displacementShader = {
      uniforms: {
        tDiffuse: { value: null },
        uNormalMap: { value: null },
      },
      vertexShader: displacementVertexShader,
      fragmentShader: displacementFragmentShader,
    }

    // GUI
    const parametersDp = {
      enabled: false,
    }
    const guiDP = gui.addFolder("displacementFilter")
    guiDP.open()

    const displacementPass = new ShaderPass(displacementShader)
    displacementPass.enabled = parametersDp.enabled
    displacementPass.material.uniforms.uNormalMap.value = textureLoader.load(`${filePath}/interfaceMap.png`)
    effectComposer.addPass(displacementPass)

    gui.add(parametersDp, "enabled").onChange(() => {
      displacementPass.enabled = parametersDp.enabled
    })

    if (useMultisampling) {
      const smaaPass = new SMAAPass()
      effectComposer.addPass(smaaPass)
    }

    // Event listeners
    window.addEventListener("dblclick", () => {
      const fullScreen = document.fullscreenElement || document.webkitfullscreenElement

      if (!fullScreen) {
        if (canvas.requestFullscreen) {
          canvas.requestFullscreen()
        } else if (canvas.webkitRequestFullscreen) {
          canvas.webkitRequestFullscreen()
        }
      } else {
        if (document.exitFullscreen) {
          document.exitFullscreen()
        } else if (document.webkitExitFullscreen) {
          document.webkitExitFullscreen()
        }
      }
    })

    window.addEventListener("resize", () => {
      sizes.width = window.innerWidth
      sizes.height = window.innerHeight

      camera.aspect = sizes.width / sizes.height
      camera.updateProjectionMatrix()

      renderer.setSize(sizes.width, sizes.height)
      renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

      effectComposer.setSize(sizes.width, sizes.height)
      effectComposer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
    })

    // Animate
    const tick = () => {
      controls.update()
      effectComposer.render()

      window.requestAnimationFrame(tick)
    }

    tick()
  }, [])

  return <canvas className="space-helmet"></canvas>
}

export default SpaceHelmet
