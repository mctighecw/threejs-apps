import React from "react"
import { Link } from "react-router-dom"

import ProjectBox from "./ProjectBox/ProjectBox"
import Footer from "./Footer/Footer"

import projects from "Constants/projects"
import "./styles.less"

const Main = () => (
  <div className="main-page">
    <div className="heading">
      Three.js <span>Apps</span>
    </div>
    <p className="paragraph margin-bottom">
      Here are some apps I've made with the JavaScript{" "}
      <a href="https://get.webgl.org" target="__blank">
        WebGL
      </a>{" "}
      library{" "}
      <a href="https://threejs.org" target="__blank">
        Three.js
      </a>
      .
    </p>

    <div className="project-grid">
      {projects.map((item, index) => (
        <ProjectBox key={index} info={item} />
      ))}
    </div>
    <p className="paragraph margin-top">
      (* made during the{" "}
      <a href="https://threejs-journey.xyz" target="__blank">
        Three.js Journey
      </a>{" "}
      course)
    </p>

    <Footer />
  </div>
)

export default Main
