import React from "react"
import "./styles.less"

const ProjectBox = (props) => {
  const { title, description, url, image } = props.info

  const handleClick = () => {
    window.open(url)
  }

  return (
    <div className="project-box" onClick={handleClick}>
      <div className="title">{title}</div>
      <img src={image} className="image" />
      <div className="description">{description}</div>
    </div>
  )
}

export default ProjectBox
