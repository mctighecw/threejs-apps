import React from "react"
import "./styles.less"

const Footer = () => (
  <div className="footer">
    &copy; 2021 Christian McTighe. Coded by Hand.
  </div>
)

export default Footer
