import React, { useEffect } from "react"
import * as THREE from "three"
import * as dat from "dat.gui"
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js"

const SpiralGalaxy = () => {
  const body = document.querySelector("body")
  body.style.overflow = "hidden"

  useEffect(() => {
    // Canvas
    const canvas = document.querySelector("canvas.galaxy")

    // Scene
    const scene = new THREE.Scene()

    // Sizes
    const sizes = {
      width: window.innerWidth,
      height: window.innerHeight,
    }

    // Renderer
    const renderer = new THREE.WebGLRenderer({
      canvas: canvas,
    })
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

    // GUI
    const gui = new dat.GUI()

    const parameters = {
      count: 100_000,
      size: 0.01,
      radius: 5,
      branches: 3,
      spin: 1,
      randomness: 0.2,
      randomnessPower: 3,
      insideColor: "#ff6030",
      outsideColor: "#1b3984",
    }

    let geometry = null
    let material = null
    let points = null

    const generateGalaxy = () => {
      // Clear old values
      if (points !== null) {
        geometry.dispose()
        material.dispose()
        scene.remove(points)
      }

      // Geometry
      geometry = new THREE.BufferGeometry()

      const positions = new Float32Array(parameters.count * 3)
      const colors = new Float32Array(parameters.count * 3)

      // Colors
      const colorInside = new THREE.Color(parameters.insideColor)
      const colorOutside = new THREE.Color(parameters.outsideColor)

      for (let i = 0; i < parameters.count; i++) {
        const i3 = i * 3

        // Position
        const radius = Math.random() * parameters.radius
        const spinAngle = radius * parameters.spin

        // Calculate angle for branches
        const branchAngle = ((i % parameters.branches) / parameters.branches) * Math.PI * 2

        const randomPositionValue = () =>
          Math.pow(Math.random(), parameters.randomnessPower) *
          (Math.random() < 0.5 ? 1 : -1) *
          parameters.randomness *
          radius

        const randomX = randomPositionValue()
        const randomY = randomPositionValue()
        const randomZ = randomPositionValue()

        // Positions (x, y, z)
        positions[i3] = Math.cos(branchAngle + spinAngle) * radius + randomX
        positions[i3 + 1] = randomY
        positions[i3 + 2] = Math.sin(branchAngle + spinAngle) * radius + randomZ

        // Color
        const mixedColor = colorInside.clone()
        mixedColor.lerp(colorOutside, radius / parameters.radius)

        colors[i3] = mixedColor.r
        colors[i3 + 1] = mixedColor.g
        colors[i3 + 2] = mixedColor.b
      }

      geometry.setAttribute("position", new THREE.BufferAttribute(positions, 3))
      geometry.setAttribute("color", new THREE.BufferAttribute(colors, 3))

      // Material
      material = new THREE.PointsMaterial({
        size: parameters.size,
        sizeAttenuation: true,
        depthWrite: false,
        blending: THREE.AdditiveBlending,
        vertexColors: true,
      })

      // Points
      points = new THREE.Points(geometry, material)
      scene.add(points)
    }
    generateGalaxy()

    // Controls panel
    gui.add(parameters, "count").min(100).max(1000000).step(100).onFinishChange(generateGalaxy)
    gui.add(parameters, "size").min(0.001).max(0.1).step(0.001).onFinishChange(generateGalaxy)
    gui.add(parameters, "radius").min(0.01).max(20).step(0.01).onFinishChange(generateGalaxy)
    gui.add(parameters, "branches").min(2).max(20).step(1).onFinishChange(generateGalaxy)
    gui.add(parameters, "spin").min(-5).max(5).step(0.001).onFinishChange(generateGalaxy)
    gui.add(parameters, "randomness").min(0).max(2).step(0.001).onFinishChange(generateGalaxy)
    gui.add(parameters, "randomnessPower").min(1).max(10).step(0.001).onFinishChange(generateGalaxy)
    gui.addColor(parameters, "insideColor").onFinishChange(generateGalaxy)
    gui.addColor(parameters, "outsideColor").onFinishChange(generateGalaxy)

    // Base camera
    const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.1, 100)
    camera.position.x = 3
    camera.position.y = 3
    camera.position.z = 3
    scene.add(camera)

    // Controls
    const controls = new OrbitControls(camera, canvas)
    controls.enableDamping = true

    // Event listeners
    window.addEventListener("dblclick", () => {
      const fullScreen = document.fullscreenElement || document.webkitfullscreenElement

      if (!fullScreen) {
        if (canvas.requestFullscreen) {
          canvas.requestFullscreen()
        } else if (canvas.webkitRequestFullscreen) {
          canvas.webkitRequestFullscreen()
        }
      } else {
        if (document.exitFullscreen) {
          document.exitFullscreen()
        } else if (document.webkitExitFullscreen) {
          document.webkitExitFullscreen()
        }
      }
    })

    window.addEventListener("resize", () => {
      sizes.width = window.innerWidth
      sizes.height = window.innerHeight

      camera.aspect = sizes.width / sizes.height
      camera.updateProjectionMatrix()

      renderer.setSize(sizes.width, sizes.height)
      renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
    })

    // Animate
    const tick = () => {
      controls.update()
      renderer.render(scene, camera)

      window.requestAnimationFrame(tick)
    }

    tick()
  }, [])

  return <canvas className="galaxy"></canvas>
}

export default SpiralGalaxy
