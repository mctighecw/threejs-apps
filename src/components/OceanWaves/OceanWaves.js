import React, { useEffect } from "react"
import * as THREE from "three"
import * as dat from "dat.gui"
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js"

import waterVertexShader from "./shaders/vertex.glsl"
import waterFragmentShader from "./shaders/fragment.glsl"

const OceanWaves = () => {
  const body = document.querySelector("body")
  body.style.overflow = "hidden"

  useEffect(() => {
    // Canvas
    const canvas = document.querySelector("canvas.ocean-waves")

    // Scene
    const scene = new THREE.Scene()

    // Sizes
    const sizes = {
      width: window.innerWidth,
      height: window.innerHeight,
    }

    // GUI
    const gui = new dat.GUI()
    const guiBW = gui.addFolder("bigWaves")
    const guiSW = gui.addFolder("smallWaves")
    const guiCo = gui.addFolder("colors")

    const parameters = {
      surfaceColor: "#9bd8ff",
      depthColor: "#186691",
    }

    /**
     * Water
     */
    // Geometry
    const waterGeometry = new THREE.PlaneGeometry(2, 2, 512, 512)

    // Material
    const waterMaterial = new THREE.ShaderMaterial({
      vertexShader: waterVertexShader,
      fragmentShader: waterFragmentShader,
      uniforms: {
        uTime: { value: 0 },
        // Big waves
        uBigWavesElevation: { value: 0.19 },
        uBigWavesFrequency: { value: new THREE.Vector2(3.23, 1.28) },
        uBigWavesSpeed: { value: 1.13 },
        // Small waves
        uSmallWavesElevation: { value: 0.11 },
        uSmallWavesFrequency: { value: 2.2 },
        uSmallWavesSpeed: { value: 0.3 },
        uSmallWavesIterations: { value: 3 },
        // Colors
        uSurfaceColor: { value: new THREE.Color(parameters.surfaceColor) },
        uDepthColor: { value: new THREE.Color(parameters.depthColor) },
        uColorOffset: { value: 0.05 },
        uColorMultiplier: { value: 2.7 },
      },
    })

    /**
     * GUI panel
     */
    // Big waves
    guiBW.add(waterMaterial.uniforms.uBigWavesElevation, "value").min(0).max(1).step(0.001).name("elevation")

    guiBW
      .add(waterMaterial.uniforms.uBigWavesFrequency.value, "x")
      .min(0)
      .max(10)
      .step(0.001)
      .name("frequencyX")

    guiBW
      .add(waterMaterial.uniforms.uBigWavesFrequency.value, "y")
      .min(0)
      .max(10)
      .step(0.001)
      .name("frequencyY")

    guiBW.add(waterMaterial.uniforms.uBigWavesSpeed, "value").min(0).max(5).step(0.001).name("speed")

    guiBW.open()

    // Small waves
    guiSW
      .add(waterMaterial.uniforms.uSmallWavesElevation, "value")
      .min(0)
      .max(1)
      .step(0.001)
      .name("elevation")

    guiSW
      .add(waterMaterial.uniforms.uSmallWavesFrequency, "value")
      .min(0)
      .max(30)
      .step(0.001)
      .name("frequency")

    guiSW.add(waterMaterial.uniforms.uSmallWavesSpeed, "value").min(0).max(4).step(0.001).name("speed")

    guiSW.add(waterMaterial.uniforms.uSmallWavesIterations, "value").min(0).max(8).step(1).name("iterations")

    guiSW.open()

    // Colors
    guiCo.addColor(parameters, "surfaceColor").onChange(() => {
      waterMaterial.uniforms.uSurfaceColor.value.set(parameters.surfaceColor)
    })

    guiCo.addColor(parameters, "depthColor").onChange(() => {
      waterMaterial.uniforms.uDepthColor.value.set(parameters.depthColor)
    })

    guiCo.add(waterMaterial.uniforms.uColorOffset, "value").min(0).max(1).step(0.001).name("offset")

    guiCo.add(waterMaterial.uniforms.uColorMultiplier, "value").min(0).max(10).step(0.001).name("multiplier")

    guiCo.open()

    // Mesh
    const water = new THREE.Mesh(waterGeometry, waterMaterial)
    water.rotation.x = -Math.PI * 0.5
    scene.add(water)

    // Event listeners
    window.addEventListener("dblclick", () => {
      const fullScreen = document.fullscreenElement || document.webkitfullscreenElement

      if (!fullScreen) {
        if (canvas.requestFullscreen) {
          canvas.requestFullscreen()
        } else if (canvas.webkitRequestFullscreen) {
          canvas.webkitRequestFullscreen()
        }
      } else {
        if (document.exitFullscreen) {
          document.exitFullscreen()
        } else if (document.webkitExitFullscreen) {
          document.webkitExitFullscreen()
        }
      }
    })

    window.addEventListener("resize", () => {
      sizes.width = window.innerWidth
      sizes.height = window.innerHeight

      camera.aspect = sizes.width / sizes.height
      camera.updateProjectionMatrix()

      renderer.setSize(sizes.width, sizes.height)
      renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
    })

    // Base camera
    const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.1, 100)
    camera.position.set(1, 1, 1)
    scene.add(camera)

    // Controls
    const controls = new OrbitControls(camera, canvas)
    controls.enableDamping = true

    // Renderer
    const renderer = new THREE.WebGLRenderer({
      canvas: canvas,
    })
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

    // Animate
    const clock = new THREE.Clock()

    const tick = () => {
      const elapsedTime = clock.getElapsedTime()

      // Update water
      waterMaterial.uniforms.uTime.value = elapsedTime

      controls.update()
      renderer.render(scene, camera)

      window.requestAnimationFrame(tick)
    }

    tick()
  }, [])

  return <canvas className="ocean-waves"></canvas>
}

export default OceanWaves
