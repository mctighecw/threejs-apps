import React, { useEffect } from "react"
import * as THREE from "three"
import * as dat from "dat.gui"
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls"
import { EffectComposer } from "three/examples/jsm/postprocessing/EffectComposer"
import { RenderPass } from "three/examples/jsm/postprocessing/RenderPass"
import CustomGlitchPass from "Custom/CustomGlitchPass"

import { randomDecimalNumber, generateGridLines, generateStars, loadMountainsSvg } from "./functions"

import VectorMountains from "Assets/images/vector-mountains.svg"
import { PINK, BLUE } from "Stylesheets/colors"

const LaserGrid = () => {
  const body = document.querySelector("body")
  body.style.overflow = "hidden"

  useEffect(() => {
    // Canvas
    const canvas = document.querySelector("canvas.laser-grid")

    // Scene
    const scene = new THREE.Scene()

    // Sizes
    const sizes = {
      width: window.innerWidth,
      height: window.innerHeight,
    }

    // Controls
    let controls = null

    // GUI
    const gui = new dat.GUI()

    const parameters = {
      enableGlitches: false,
      freezeCamera: true,
      lineColor: PINK,
      toggleLineColor: () => null,
    }

    // Grid lines
    const lineMaterial = new THREE.LineBasicMaterial({ color: parameters.lineColor })
    const lines = generateGridLines(-110, 110, 5, lineMaterial)
    scene.add(...lines)

    gui.add(parameters, "toggleLineColor").onChange(() => {
      const lineColor = parameters.lineColor === PINK ? BLUE : PINK
      parameters.lineColor = lineColor
      lineMaterial.color.set(lineColor)
    })

    // Stars
    const stars = generateStars(200, false)
    scene.add(stars)

    // Mountains
    const mountains = loadMountainsSvg(VectorMountains)
    scene.add(mountains)

    // Camera
    const camera = new THREE.PerspectiveCamera(45, sizes.width / sizes.height, 0.01, 1000)
    camera.position.set(0, -100, 16)
    camera.lookAt(0, 0, 0)

    gui.add(parameters, "freezeCamera").onChange(() => {
      if (controls === null) {
        controls = new OrbitControls(camera, canvas)
        controls.enableDamping = true

        // zoom in/out distances
        controls.minDistance = 20
        controls.maxDistance = 100

        // forward/backward tilt
        controls.minPolarAngle = 2.9549
        controls.maxPolarAngle = 3.0426

        // left/right tilt
        controls.minAzimuthAngle = -0.146
        controls.maxAzimuthAngle = 0.146
      }

      controls.enabled = !parameters.freezeCamera
    })

    // Renderer
    const renderer = new THREE.WebGLRenderer({ canvas })
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
    renderer.render(scene, camera)

    // Post processing
    const effectComposer = new EffectComposer(renderer)
    effectComposer.setSize(sizes.width, sizes.height)
    effectComposer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

    // Passes
    const renderPass = new RenderPass(scene, camera)
    effectComposer.addPass(renderPass)

    const glitchPass = new CustomGlitchPass()
    glitchPass.glitchFrequency = randomDecimalNumber(0.2, 0.6) // random value
    glitchPass.enabled = parameters.enableGlitches
    effectComposer.addPass(glitchPass)

    gui.add(parameters, "enableGlitches").onChange(() => {
      glitchPass.glitchFrequency = randomDecimalNumber(0.2, 0.6)
      glitchPass.enabled = parameters.enableGlitches
    })

    // Event listeners
    window.addEventListener("resize", () => {
      sizes.width = window.innerWidth
      sizes.height = window.innerHeight

      camera.aspect = sizes.width / sizes.height
      camera.updateProjectionMatrix()

      renderer.setSize(sizes.width, sizes.height)
      renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
    })

    window.addEventListener("dblclick", () => {
      const fullScreen = document.fullscreenElement || document.webkitfullscreenElement

      if (!fullScreen) {
        if (canvas.requestFullscreen) {
          canvas.requestFullscreen()
        } else if (canvas.webkitRequestFullscreen) {
          canvas.webkitRequestFullscreen()
        }
      } else {
        if (document.exitFullscreen) {
          document.exitFullscreen()
        } else if (document.webkitExitFullscreen) {
          document.webkitExitFullscreen()
        }
      }
    })

    // Animate
    const tick = () => {
      if (controls !== null) {
        controls.update()

        // Adjust mountains rotation
        const newAngle = controls.getAzimuthalAngle()
        mountains.material.rotation = newAngle
      }

      effectComposer.render()

      window.requestAnimationFrame(tick)
    }
    tick()
  }, [])

  return <canvas className="laser-grid"></canvas>
}

export default LaserGrid
