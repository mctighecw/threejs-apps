import * as THREE from "three"
import { OFF_WHITE } from "Stylesheets/colors"

import starsData from "./data/starsData.json"

export const randomDecimalNumber = (min, max) => {
  const minVal = min * 10
  const maxVal = max * 10
  let res = Math.random() * (maxVal - minVal + 1) + minVal
  res /= 10
  res = res.toFixed(2)
  res = Number(res)
  return res
}

export const generateGridLines = (minVal, maxVal, step, material) => {
  const lines = []

  // Vertical lines
  for (let x = minVal; x <= maxVal; x += step) {
    const points = []
    points.push(new THREE.Vector3(x, maxVal, 0))
    points.push(new THREE.Vector3(x, minVal, 0))

    const geometry = new THREE.BufferGeometry().setFromPoints(points)
    const line = new THREE.Line(geometry, material)
    lines.push(line)
  }

  // Horizontal lines
  for (let y = minVal; y <= maxVal; y += step) {
    const points = []
    points.push(new THREE.Vector3(minVal, y, 0))
    points.push(new THREE.Vector3(maxVal, y, 0))

    const geometry = new THREE.BufferGeometry().setFromPoints(points)
    const line = new THREE.Line(geometry, material)
    lines.push(line)
  }

  return lines
}

export const generateStars = (count, useRandom) => {
  // Geometry
  const particlesGeometry = new THREE.BufferGeometry()
  const positions = new Float32Array(count * 3)

  for (let i = 0; i < count; i++) {
    const i3 = i * 3

    if (useRandom) {
      // Create new random positions
      positions[i3] = (Math.random() - 0.5) * 190
      positions[i3 + 1] = (Math.random() + 2) * 20
      positions[i3 + 2] = Math.random() * (50 - 16) + 16 // range: 16-50
    } else {
      // Use positions from json file
      positions[i3] = starsData[i3]
      positions[i3 + 1] = starsData[i3 + 1]
      positions[i3 + 2] = starsData[i3 + 2]
    }
  }
  particlesGeometry.setAttribute("position", new THREE.BufferAttribute(positions, 3))

  // Material
  const particlesMaterial = new THREE.PointsMaterial()
  particlesMaterial.size = 0.6
  particlesMaterial.sizeAttenuation = true
  particlesMaterial.color = new THREE.Color(OFF_WHITE)
  particlesMaterial.transparent = true
  particlesMaterial.depthWrite = false

  const particles = new THREE.Points(particlesGeometry, particlesMaterial)
  return particles
}

export const loadMountainsSvg = (svg) => {
  const map = new THREE.TextureLoader().load(svg)
  const material = new THREE.SpriteMaterial({
    map,
  })

  const sprite = new THREE.Sprite(material)
  sprite.position.set(0, 290, 1)
  sprite.scale.set(410, 30, 1)

  return sprite
}
