import React from "react"
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom"

import Main from "Components/Main/Main"
import SpiralGalaxy from "Components/SpiralGalaxy/SpiralGalaxy"
import OceanWaves from "Components/OceanWaves/OceanWaves"
import SpaceHelmet from "Components/SpaceHelmet/SpaceHelmet"
import MagicPortal from "Components/MagicPortal/MagicPortal"
import LaserGrid from "Components/LaserGrid/LaserGrid"

import "./styles/global.css"

const App = () => (
  <Router>
    <Switch>
      <Route exact path="/" component={Main} />
      <Route path="/galaxy" component={SpiralGalaxy} />
      <Route path="/ocean-waves" component={OceanWaves} />
      <Route path="/space-helmet" component={SpaceHelmet} />
      <Route path="/magic-portal" component={MagicPortal} />
      <Route path="/laser-grid" component={LaserGrid} />
      <Route render={() => <Redirect to="/" />} />
    </Switch>
  </Router>
)

export default App
