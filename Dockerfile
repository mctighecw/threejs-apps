FROM node:16-slim

WORKDIR /usr/src/app

COPY . .
RUN yarn install --silent
RUN yarn build --silent

CMD ["node", "server.js"]
