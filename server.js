const path = require("path")
const express = require("express")
const morgan = require("morgan")

const app = express()
const port = process.env.PORT || 5000

app.use(morgan("common"))
app.use(express.static(path.resolve(__dirname, "build")))

app.get("/*", (req, res) => {
  res.sendFile(path.resolve(__dirname, "build", "index.html"))
})

app.listen(port, () => {
  console.log(`Node server is running on port ${port}`)
})

process.on("SIGINT", () => {
  console.info("Stopping Node server")
  process.exit(0)
})
