# README

Various apps made with the JavaScript [WebGL](https://get.webgl.org) 3D library, [Three.js](https://threejs.org). These apps were made during and after a course, [Three.js Journey](https://threejs-journey.xyz).

_Note_: This is the first project where I have not used semicolons in the JavaScript.

## App Information

App Name: threejs-apps

Created: March-April 2021

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/threejs-apps)

Production: [Link](https://threejs.mctighecw.site)

## Tech Stack

- React
- Three.js
- Custom shaders (GLSL)
- Less & CSS
- Webpack
- Prettier
- Node.js Express server

## To Run

_Development_

```
$ yarn install
$ yarn start
```

_Production_

1. Terminal

```
$ yarn install
$ yarn build
$ node server.js
```

2. Docker

```
$ docker build -t threejs-apps .
$ docker run --rm -it -p 80:5000 threejs-apps
```

## Credits

_General_:

- Three.js [favicon](https://threejs.org)
- Basier Circle Regular font

_Space Helmet_ App:

- Battle Damaged Sci-fi Helmet by [theblueturtle](https://sketchfab.com/theblueturtle_)
- Background image "Kiara 5 Noon" by Greg Zaal (CC0 license) from [HDRIHaven](https://hdrihaven.com/hdri/?h=kiara_5_noon)

Last updated: 2025-01-30
