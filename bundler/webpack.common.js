const path = require("path")
const HTMLWebpackPlugin = require("html-webpack-plugin")

const src = path.resolve(__dirname, "../src")

module.exports = {
  entry: path.resolve(src, "index.js"),
  output: {
    filename: "bundle.[id].js",
    path: path.resolve(__dirname, "../build"),
    libraryTarget: "umd",
    publicPath: "/",
  },
  resolve: {
    extensions: [".js", ".jsx"],
    alias: {
      Assets: path.resolve(src, "assets"),
      Components: path.resolve(src, "components"),
      Constants: path.resolve(src, "constants"),
      Custom: path.resolve(src, "custom"),
      Stylesheets: path.resolve(src, "styles"),
      LessConstants: path.resolve(src, "styles/constants.less"),
    },
  },
  plugins: [
    new HTMLWebpackPlugin({
      template: path.resolve(src, "index.html"),
      favicon: path.resolve(src, "assets/favicon.ico"),
    }),
  ],
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: "babel-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.(ico|jpg|png|gif|svg|glb)$/,
        use: "file-loader?name=assets/[name].[ext]",
      },
      {
        test: /\.(ttf|eot|woff|woff2)$/,
        use: "file-loader?name=assets/fonts/[name].[ext]",
      },
      {
        test: /\.(glsl|vs|fs|vert|frag)$/,
        use: "raw-loader",
        exclude: /node_modules/,
      },
    ],
  },
}
