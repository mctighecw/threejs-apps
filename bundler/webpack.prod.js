const webpack = require("webpack")
const CopyPlugin = require("copy-webpack-plugin")
const MiniCSSExtractPlugin = require("mini-css-extract-plugin")
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin")
const TerserPlugin = require("terser-webpack-plugin")
const { merge } = require("webpack-merge")
const common = require("./webpack.common.js")

module.exports = merge(common, {
  mode: "production",
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [MiniCSSExtractPlugin.loader, "css-loader"],
      },
      {
        test: /\.less$/,
        use: [MiniCSSExtractPlugin.loader, "css-loader", "less-loader"],
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify("production"),
      },
    }),
    new MiniCSSExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css",
    }),
    new CopyPlugin({
      patterns: [
        {
          from: "src/components/SpaceHelmet/files",
          to: "assets",
          toType: "dir",
        },
        {
          from: "node_modules/three/examples/js/libs/draco",
          to: "draco",
          toType: "dir",
        },
      ],
    }),
  ],
  optimization: {
    namedModules: true,
    namedChunks: true,
    minimizer: [
      new OptimizeCSSAssetsPlugin(),
      new TerserPlugin({
        extractComments: true,
        cache: true,
        parallel: true,
        sourceMap: true,
        terserOptions: {
          ecma: 6,
          warnings: false,
          mangle: true,
          module: false,
          toplevel: false,
          ie8: false,
          keep_fnames: false,
          safari10: false,
          extractComments: "all",
          compress: {
            drop_console: true,
          },
        },
      }),
    ],
  },
})
